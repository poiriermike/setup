# Setup

This repository sets up a fresh Ubuntu system with the basics necessary for JavaScript and Python development with the latest Docker Community Edition.

DISCLAIMER: `Configuration of your system using this tooling requires SUDO priviliges. It WILL add repositories and install packages onto your system. Please review what this code is doing before executing it. Don't blindly run my scripts. Who knows, I could silently be installing a BitCoin miner on your system, or overriding DNS in your system to point everything to naughty sites.`

## Requirements
* Ubuntu 16.04+
* Python 3

## Usage

Run `setup.sh` as your your local account. You will be prompted for sudo credentials twice during the script. Once to download the bootstrap packages required to run Ansible, and second to execute the Ansible playbook with sudo privliges.

If you already have your virtual environment configuration set up in a way that's different than mine, you can just run the Ansible tooling for the rest of the environment by running `ansible-playbook setup.yml --ask-become-pass`

## What It Does

* Installs node and npm
* Installs pip, pipsi (encapsulating pip installs for executables such as ansible/docker-compose), and pew (a nice wrapper for python virtualenvs)
* Installs some useful Linux utilities for both network and debugging
* Installs and starts the Docker agent allowing for Docker-based development with the latest and greatest version
