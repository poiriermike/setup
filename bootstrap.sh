#! /bin/bash

if [[ "${USER}" == "root" ]]; then
    echo "This script should be run as a non-priviliged user."
    echo "It will prompt for sudo password as it installs packages,"
fi

# Intial setup script to bootstrap Ansible configuration

echo "Bootstrapping installation"
# Install Pip
sudo apt-get update
sudo apt-get install -y python3-pip python3-apt python3-venv pipx

pipx ensurepath

# Install required packages for bootstrapping
which ansible &> /dev/null
if [[ $? -ne 0 ]]; 
then
    pipx install ansible --include-deps
fi

echo "Pipsi, pew, and ansible installed. Beginning ansible configuration."
echo ""
# Evals are generally bad form in bash as it can lead to arbitratry code execution
# Since I'm not actually taking user input directly, this is fine.
eval "${HOME}/.local/pipx/venvs/ansible/bin/ansible-playbook setup.yml -u "${USER}" --ask-become-pass"
